package be.ucll.java.ent.view;

import be.ucll.java.ent.controller.UserController;
import be.ucll.java.ent.utils.BeanUtil;
import com.vaadin.flow.component.*;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.shared.Registration;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Route("")
@PageTitle("StuBS")
class MainView extends AppLayout implements BeforeEnterObserver{

    // Content views
    private StudentenView sView;
    private LeermodulesView lmView;
    private Button btnLogout;

    private UserController userController;

    // Left navigation tabs
    private Tab tab1;
    private static final String TABNAME1 = "Studenten";
    private Tab tab2;
    private static final String TABNAME2 = "Leermodules";
    private Tab tab3;
    private Tab tab4;
    private Tabs tabs;


    public MainView() {
        // Header / Menu bar on the top of the page
        userController = BeanUtil.getBean(UserController.class);
        H3 header = new H3("Stubs - Het STUdenten Beheer Systeem");


        addToNavbar(new DrawerToggle(),
                new Html("<span>&nbsp;&nbsp;</span>"),
                header,
                new Html("<span>&nbsp;&nbsp;</span>")
                );

        // Tabs on the left side drawer
        tab1 = new Tab(TABNAME1);
        tab2 = new Tab(TABNAME2);
        tab3 = new Tab("Help");
        tab3.setEnabled(false);
        tab4 = new Tab(new Icon(VaadinIcon.COG));

        tabs = new Tabs(tab1, tab2, tab3, tab4);
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.addSelectedChangeListener(event -> {
            handleTabClicked(event);
        });
        addToDrawer(tabs);
        getUI().ifPresent(ui -> ui.navigate("stubs3"));
    }



    @PostConstruct
    private void setMainViewContent() {
        sView = new StudentenView();
        sView.loadData();

        lmView = new LeermodulesView();
        lmView.loadData();

        // As default load the studentenview
        this.setContent(sView);
    }

    private void handleTabClicked(Tabs.SelectedChangeEvent event) {
        Tab selTab = tabs.getSelectedTab();
        if (selTab.getLabel() != null) {
            if (selTab.getLabel().equals(TABNAME1)) {
                setContent(sView);
            } else if (selTab.getLabel().equals(TABNAME2)) {
                setContent(lmView);
            } else {
                setContent(new Label("Te implementeren scherm voor Admins only"));
            }
        }
    }


    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if (!userController.isUserSignedIn())
        {
            event.rerouteTo("stubs3");
        }
    }
}
